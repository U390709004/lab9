package ThreeDimensionalShape;

public class Tetrahedron extends ThreeDimensionalShape {
private double side;
public Tetrahedron(double side){
    this.side = side;
}

    public double getArea() {
        return (this.side*this.side*Math.sqrt(3));


    }

    public double getVolume() {

        return (this.side * this.side * this.side/(Math.sqrt(2)*6));
    }



}