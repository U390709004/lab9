package ThreeDimensionalShape;


public class Cube extends ThreeDimensionalShape {

    private int side;


    public Cube(int side) {
        super();
        this.side = side;
    }

    public double getArea() {

        return (this.side*this.side*6);

    }

    public double getVolume() {
        return Math.pow(this.side,3);

    }



}