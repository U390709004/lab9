package ThreeDimensionalShape;

public class Sphere extends ThreeDimensionalShape {

    private double radius;

    public Sphere(double radius) {
        super();
        this.radius = radius;
    }


    public double getArea() {
        return (4*Math.PI*(Math.pow(this.radius,2)));
    }

    public double getVolume() {
        return (4*Math.PI *(Math.pow(this.radius,3)*4)/3.0);
    }



}