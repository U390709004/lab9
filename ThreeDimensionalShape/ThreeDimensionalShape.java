package ThreeDimensionalShape;
import Sphare.Shape;

public abstract class ThreeDimensionalShape implements Shape {

    public double getArea() {
        return 0.0;
    }


    public double getVolume() {
        return 0.0;
    }



    public String toString(){
        return String.format("Area: " + getArea()+ " Volume:" + getVolume());
    }

}