import java.util.ArrayList;
import java.util.List;
import TwoDimensionalShape.*;
import ThreeDimensionalShape.*;
import Sphare.*;

public class Tester {

    public static void main(String[] args) {

        List<Shape> list = new ArrayList<Shape>();

        Shape C = new Circle(5);
        Shape S = new Square(1.9);
        Shape T = new Triangle(3,4,5,4.7);

        Sphere SP = new Sphere(7);
        Cube CU = new Cube(3);
        Tetrahedron TE = new Tetrahedron(2.5);

        double TA = 0;
        double TP = 0;
        double TV = 0;

        list.add((Shape) C);
        list.add((Shape) S);
        list.add((Shape) T);
        list.add((Shape) SP);
        list.add((Shape) CU);
        list.add((Shape) TE);

        for (Shape shape : list){
            TA += shape.getArea();
        }

        for (Shape shape : list){
            if (shape instanceof TwoDimensionalShape){
                System.out.println(shape.getClass().getSimpleName() +" " + shape.toString());
                TP += ((TwoDimensionalShape) shape).getPerimeter();
            }
        }

        for (Shape shape : list){
            if (shape instanceof ThreeDimensionalShape){
                System.out.println(shape.getClass().getSimpleName() +" " + shape.toString());
                TV += ((ThreeDimensionalShape) shape).getVolume();
            }
        }
        System.out.println("Total Area: " + TA);
        System.out.println("Total Perimeter: " + TP);
        System.out.println("Total Volume: " + TV);


    }



}