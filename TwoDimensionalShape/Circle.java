package TwoDimensionalShape;

public class Circle extends TwoDimensionalShape {

    private double radius;

    public Circle(double radius) {
        super();
        this.radius = radius;

    }

    public double getArea() {

        return (Math.PI * this.radius * this.radius);
    }


    public double getPerimeter() {

        return (Math.PI * 2 *this.radius);
    }



}