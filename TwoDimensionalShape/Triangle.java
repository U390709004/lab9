package TwoDimensionalShape;

public class Triangle extends TwoDimensionalShape {

    private double side1;
    private double side2;
    private double side3;
    private double height;




    public Triangle(double side1,double side2,double side3, double height) {
        super();
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.height = height;
    }

    public double getArea() {
        double half = (this.side1+this.side2+this.side3)/2;
        return  Math.sqrt(half*(half-side1)*(half-side2)*(half-side3));

    }
}