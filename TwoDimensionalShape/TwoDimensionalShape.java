package TwoDimensionalShape;
import Sphare.Shape;

public abstract class TwoDimensionalShape implements Shape {

    public double getArea() {
        return 0.0;
    }

    public double getPerimeter() {
        return 0.0;
    }

    public String toString(){
        return String.format("Area: " + getArea()+ " Perimeter:" + getPerimeter());
    }

}