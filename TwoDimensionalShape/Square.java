package TwoDimensionalShape;

public class Square extends TwoDimensionalShape {

    private double side;



    public Square(double side) {
        super();
        this.side = side;
    }

    public double getArea() {
        return (this.side * this.side);
    }

    public double getPerimeter(){
        return 4*this.side;
    }



}